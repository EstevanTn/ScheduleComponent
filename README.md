# Schedule component
> VueJS component

## Instructions
- Install dependences `npm install`
- Start local server `npm run dev`

## Screenshot

![screenshot](capture.png)

---

> [License](LICENSE.md) \
[EstevanTn](https://tunaqui.com)