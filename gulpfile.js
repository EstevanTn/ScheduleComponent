var gulp = require('gulp');
var plumber = require('gulp-plumber');
var pug = require('gulp-pug');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var cssnano = require('cssnano');
var watch = require('gulp-watch');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var coffee = require('gulp-coffee');

const server = browserSync.create();

const postcssPlugins = [
  cssnano({
    core: false,
    autoprefixer: {
      add: true,
      browsers: '> 1%, last 2 versions, Firefox ESR, Opera 12.1'
    }
  })
];

const sassOptions = {
  outputStyle: 'expanded'
};

gulp.task('styles', () =>
  gulp.src('./src/scss/app.scss')
  .pipe(sourcemaps.init({ loadMaps: true }))
  .pipe(plumber())
  .pipe(sass(sassOptions))
  .pipe(postcss(postcssPlugins))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('./public/css'))
  .pipe(server.stream({match: '**/*.css'}))
);

gulp.task('pug', () =>
  gulp.src('./src/pug/pages/*.pug')
  .pipe(plumber())
  .pipe(pug())
  .pipe(gulp.dest('./public'))
);

gulp.task('scripts', () =>
  browserify({
    entries: ['./src/js/index.js'],
    debug: true,
    transform: ['vueify']
  })
  .transform(babelify, { presets: ['es2015'] })
  .bundle()
  .on('error', function(err){
    console.error(err);
    this.emit('end')
  })
  .pipe(source('bundle.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({ loadMaps: true }))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('./public/js'))
);

gulp.task('coffee', () => 
  gulp.src('./src/coffee/**/*.coffee')
  .pipe(plumber())
  .pipe(coffee())
  .pipe(gulp.dest('./src/js'))
);

gulp.task('default', () => {
  server.init({
    server: {
      baseDir: './public'
    },
  });
  watch('./src/scss/**/*.scss', () => gulp.start('styles'));
  watch('./src/coffee/**/*.coffee', () => gulp.start('coffee'));
  watch('./src/js/**/*.js', () => gulp.start('scripts',server.reload));
  watch('./src/vue/**/*.vue', () => gulp.start('scripts',server.reload));
  watch('./src/pug/**/*.pug', () => gulp.start('pug', server.reload));
});
