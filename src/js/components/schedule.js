"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {};
    },

    props: {
        types: {
            type: Array,
            required: false
        }
    },
    methods: {
        allDays: function allDays() {},
        customDays: function customDays() {}
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"row\">\n    <div class=\"col-xs-12\">\n        <label><input type=\"radio\" @click=\"customDays\" name=\"type_schedule\" :value=\"types[0]\"> Abierto en horas concretas</label>\n        <label><input type=\"radio\" @click=\"allDays\" name=\"type_schedule\" :value=\"types[1]\"> Siempre abierto</label>\n        <label><input type=\"radio\" name=\"type_schedule\" :value=\"types[2]\"> Horario no disponible</label>\n        <label><input type=\"radio\" name=\"type_schedule\" :value=\"types[3]\"> Cerrado definitivamente</label>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  if (!module.hot.data) {
    hotAPI.createRecord("_v-407d96c7", module.exports)
  } else {
    hotAPI.update("_v-407d96c7", module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}