###
import jquery from 'jquery'
import 'popper.js'
import 'bootstrap'
global.$ = jquery
###

Vue.component 'schedule-restaurant', require '../vue/components/schedule.vue'

schedule = new Vue {
    el: '#wrapper'
    data: {
        options: [
            {
                value: 'custom', text: 'Abierto ciertos días',
                message: 'Personaliza tu horario de atención.'
            },
            {
                value: 'all', text: 'Siempre abierto',
                message: 'El establecimiento se encuentra abierto las 24 horas.'
            },
            {
                value: 'notavailable',
                text: 'No disponible',
                message: 'El establecimiento no cuenta con un horario de atención establecido.'
            },
            {
                value: 'closed',
                text: 'Cerrado definitivamente',
                message: 'El establemiento no cuenta con atención al público.'
            },
        ]
    }
    methods: {
        saveTime: (item, parent, e) ->
            console.dir item
            return
        removeTime: (item, index, parent, e) ->
            console.log 'xx'
            return
        load: (parent) ->
            console.dir parent
    }
}